using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] Enemy enemyPrefab;
    [SerializeField] Enemy enemyGunnerPrefab;
    [SerializeField] Tower tower;

    [Space]

    [SerializeField] float waveTimer = 26;
    [SerializeField] float delayBetweenWave = 8f;
    [SerializeField] float frequencySpawn = 10;

    [SerializeField] float delayBeforeCharge = 1f;
    [SerializeField] int maxDistanceSpawn = 15;

    [Space]

    [SerializeField] AnimationCurve curva;

    public List<Enemy> enemies;

    public int Wave { get; set; } = 1;

    bool towerDestroyed;


    private void Start()
    {
        enemies = new List<Enemy>();

        tower.Init(this);

        StartCoroutine(Spawner());

        EventsHolder.onTowerDestroyed.AddListener(Tower_Destroyed);
    }

    private void Tower_Destroyed(Enemy destroyer)
    {
        towerDestroyed = true;
    }

    IEnumerator Spawner()
    {
        
        if(Wave > 10)
        {
            frequencySpawn = (int)(11 * Mathf.Log10(Wave));
        }

        var delayBetweenSpawn = waveTimer / frequencySpawn;
        //print(frequencySpawn);

        Spawn(Enemy.Kind.Boss);

        for (int i = 0; i < frequencySpawn; i++)
        {
            yield return new WaitForSeconds(delayBetweenSpawn);

            if (towerDestroyed)
                break;

            Spawn(Enemy.Kind.Basic);
            Spawn(Enemy.Kind.Fast);
            Spawn(Enemy.Kind.Tank);
            Spawn(Enemy.Kind.Gunner);
        }

        

        if (Wave % 8 == 0)
        {
            yield return new WaitForSeconds(delayBeforeCharge);

            for (int i = 0; i < frequencySpawn; i++)
            {
                yield return new WaitForSeconds(0.1f);

                if (towerDestroyed)
                    break;

                Spawn(Enemy.Kind.Basic);
                Spawn(Enemy.Kind.Fast);
                Spawn(Enemy.Kind.Tank);
            }
        }

        if (!towerDestroyed)
        {
            yield return new WaitForSeconds(delayBetweenWave);

            Wave++;

            EventsHolder.onNextWave?.Invoke(Wave);

            StartCoroutine(Spawner());
        }
    }

    void Spawn(Enemy.Kind kind)
    {
        float chance = 100;

        if(kind == Enemy.Kind.Basic)
        {
            chance = 95;
        }
        if(kind == Enemy.Kind.Fast)
        {
            chance = 10;
        }
        if (kind == Enemy.Kind.Tank)
        {
            chance = 10;
        }
        if (kind == Enemy.Kind.Gunner)
        {
            chance = 10;
        }
        if (kind == Enemy.Kind.Boss)
        {
            if (Wave % 10 == 0)
            {
                chance = 100;
            }
            else
            {
                chance = 0;
            }
        }

        if (Random.Range(0, 100) > chance)
            return;

        var prefab = enemyPrefab;
        if(kind == Enemy.Kind.Gunner)
        {
            prefab = enemyGunnerPrefab;
        }

        var pos = Random.insideUnitCircle * maxDistanceSpawn;
        var dist = Vector2.Distance(tower.transform.position, pos);

        while(dist < maxDistanceSpawn * 0.8f)
        {
            pos = Random.insideUnitCircle * maxDistanceSpawn;
            dist = Vector2.Distance(tower.transform.position, pos);
        }

        var e = Instantiate(prefab, pos, Quaternion.identity);
        e.Init(kind, tower, Wave);
        enemies.Add(e);
    }
}
