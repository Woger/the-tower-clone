using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static int money;
    public static int startMoney = 80;
    public static float moneyMultiplier = 1f;
    public static int moneyWave = 0;

    public static PlayerData data = new PlayerData();

    private void Start()
    {
        money = startMoney;

        EventsHolder.onEnemyDestroyed.AddListener(Enemy_Destroyed);
        EventsHolder.onNextWave.AddListener(Wave_Nexted);
    }

    private void Wave_Nexted(int wave)
    {
        money += (int)((float)moneyWave * moneyMultiplier);

        if(data.maxWave < wave)
        {
            data.maxWave = wave;

            EventsHolder.onNewMaxWave?.Invoke(wave);
        }

        SaveData();

        EventsHolder.onMoneyChanged?.Invoke();
    }

    private void Enemy_Destroyed(Enemy enemy)
    {
        money += (int)((float)enemy.reward * moneyMultiplier);
        print(enemy.coinsReward + " =-=-=- " + enemy.kind);
        data.coins += enemy.coinsReward ?? 0;

        SaveData();

        EventsHolder.onMoneyChanged?.Invoke();
    }

    public static void SaveData()
    {
        var str = JsonUtility.ToJson(data);
        PlayerPrefs.SetString("data", str);
        PlayerPrefs.Save();
        print(str);
    }

    public static void LoadData()
    {
        if (PlayerPrefs.HasKey("data"))
        {
            var str = PlayerPrefs.GetString("data");
            data = JsonUtility.FromJson<PlayerData>(str);
            //print(data.upgrades.Count);
            print(str);
        }
    }
}

[Serializable]
public class PlayerData
{
    public List<Upgrade> upgrades;
    public int coins;
    public int maxWave;

    public PlayerData()
    {
        upgrades = new List<Upgrade>();
        coins = 1000;
    }

    [Serializable]
    public class Upgrade
    {
        public int lvl;
        public string name;
    }

    public void SetUpgrade(UpgradeData data)
    {
        var upgrade = upgrades.Find(u => u.name == data.name.ToString());
        if(upgrade == null)
        {
            upgrades.Add(new Upgrade { name = data.name.ToString(), lvl = data.lvlWorkshop });
        }
        else
        {
            upgrade.lvl = data.lvlWorkshop;
        }
    }

    public Upgrade GetData(UpgradeData data)
    {
        return upgrades.Find(u => u.name == data.name.ToString());
    }
}
