using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGunner : Enemy
{
    [SerializeField] ProjectileEnemy projectilePrefab;

    protected override void Update()
    {
        currentRate += Time.deltaTime;

        DistanceToTower = Vector2.Distance(target.transform.position, transform.position);
        if (DistanceToTower > target.Parameters.distance)
        {
            transform.position += (Vector3)(dir * baseSpeed * speed * Time.deltaTime);
        }
        else
        {
            if (currentRate > attackRate)
            {
                var p = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
                p.Init(target, this);

                currentRate = 0;
            }
        }
    }
}
