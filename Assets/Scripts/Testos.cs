using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testos : MonoBehaviour
{
    public float a = 1f;
    public float b = 0.1f;
    public GameObject GameObjectBRO;

    void Start()
    {
        //print(34 / Mathf.Log10(600));
        //print(12.23f * Mathf.Log10(300));
        //print(12.23f * Mathf.Log10(10));
        //print(12.23f * Mathf.Log10(1000));

        for (float x = 2; x < 1000; x++)
        {
            //float y = a * Mathf.Exp(-b * x);
            //float y = a + b * Mathf.Log10(x);
            float y = Mathf.Pow(x, 1.3f);

            Vector2 pos = new Vector2(x/5, y);
            Instantiate(GameObjectBRO, pos, Quaternion.identity);
        }
    }

    
}
