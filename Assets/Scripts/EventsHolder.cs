using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class EventsHolder
{
    public class EnemyDestroyed : UnityEvent<Enemy> { }

    public static EnemyDestroyed onEnemyDestroyed = new EnemyDestroyed();

    //======================================================================

    public class MoneyChanged : UnityEvent { }

    public static MoneyChanged onMoneyChanged = new MoneyChanged();

    //======================================================================

    public class NextWave : UnityEvent<int> { }

    public static NextWave onNextWave = new NextWave();

    //======================================================================

    public class TowerDestroyed : UnityEvent<Enemy> { }

    public static TowerDestroyed onTowerDestroyed = new TowerDestroyed();

    //======================================================================

    public class NewMaxWave : UnityEvent<int> { }

    public static NewMaxWave onNewMaxWave = new NewMaxWave();

    //======================================================================
}
