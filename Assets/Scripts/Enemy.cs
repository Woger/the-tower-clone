using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Kind kind;
    [SerializeField] protected float speed = 10;
    [SerializeField] protected float baseSpeed = 1.9f;
    [SerializeField] protected float attackRate = 0.5f;
    [SerializeField] public int damage = 1;
    [SerializeField] GameObject view;

    [Space] 

    [SerializeField] AnimationCurve hape;
    [SerializeField] AnimationCurve hapeTank;
    [SerializeField] AnimationCurve rewardCurva;

    public int reward = 1;
    public int? coinsReward;

    public float DistanceToTower { get; protected set; }

    [field:SerializeField]
    public int HealthPoint { get; set; }

    protected Tower target;
    protected Vector2 dir;
    protected float currentRate;

    public virtual void Init(Kind kind, Tower tower, int wave)
    {
        target = tower;
        this.kind = kind;

        var opta = 1 + Mathf.Pow(wave, 1.31f);
        //print($"{opta:F0} Wave - {wave} Kind {kind}");

        HealthPoint = (int)opta;
        var factor = wave / 10;

        reward = 1 + (int)((float)wave / 10f);
        speed = 1 + (((float)wave / 10f) * 0.01f);

        if (factor >= 1)
        {
            HealthPoint += (int)Mathf.Pow(factor, 1.3f);
        }

        if (kind == Kind.Fast)
        {
            speed *= 2.12f;
            reward = 1 + (int)((float)wave / 10f) + (int)((float)wave / 20);
            coinsReward = 2;
        }
        if (kind == Kind.Gunner)
        {
            speed *= 2.01f;
            reward = 1 + (int)((float)wave / 10f) + (int)((float)wave / 20);
            coinsReward = 2;
        }
        if (kind == Kind.Tank)
        {
            HealthPoint *= 5;
            speed *= 0.5f;
            reward = 2 + (int)((float)wave / 10f) + (int)((float)wave / 20);
            view.transform.localScale *= 1.5f;
            coinsReward = 4;
        }
        if (kind == Kind.Boss)
        {
            HealthPoint *= 20;
            speed *= 0.3f;
            reward = 5 + ((wave / 10) * 5);
            view.transform.localScale *= 3f;
            coinsReward = 8;
        }

        dir = (target.transform.position - transform.position).normalized;

    }

    protected virtual void Update()
    {
        currentRate += Time.deltaTime;

        DistanceToTower = Vector2.Distance(target.transform.position, transform.position);
        if(DistanceToTower > 1)
        {
            transform.position += (Vector3)(dir * baseSpeed * speed * Time.deltaTime);
        }
        else
        {
            if(currentRate > attackRate)
            {
                target.Parameters.Damage(this);
                
                currentRate = 0;
            }
        }
    }

    public enum Kind
    {
        Basic,
        Fast,
        Tank,
        Boss,
        Gunner
    }
}
