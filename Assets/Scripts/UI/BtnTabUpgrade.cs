using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class BtnTabUpgrade : MonoBehaviour
{
    [SerializeField] TabUpgrade tabUpgrade;
    [SerializeField] Button button;

    public Action<TabUpgrade> onTabClick;

    public void Init()
    {
        button.onClick.AddListener(Tab_Clicked);
    }

    private void Tab_Clicked()
    {
        onTabClick?.Invoke(tabUpgrade);
    }
}

public enum TabUpgrade
{
    Attack,
    Defence,
    Economic
}
