using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelWorkshop : MonoBehaviour
{
    
    [SerializeField] BtnUpgrade btnUpgradePrefab;
    [SerializeField] Transform attackParent;
    [SerializeField] Transform defenceParent;
    [SerializeField] Transform economicParent;

    [Space(30)]

    [SerializeField] AnimationCurve curvaDamaga;

    UpgradesPack pack;

    public void Init(UpgradesPack pack)
    {
        this.pack = pack;

        SwitchTab(NavigationTab.Attack);

        InitUpgrades(pack.attack, attackParent);
        InitUpgrades(pack.defence, defenceParent);
        InitUpgrades(pack.economic, economicParent);
    }

    public void SwitchTab(NavigationTab tab)
    {
        switch (tab)
        {
           
            case NavigationTab.Attack:
                attackParent.gameObject.SetActive(true);
                defenceParent.gameObject.SetActive(false);
                economicParent.gameObject.SetActive(false);
                break;

            case NavigationTab.Defence:
                attackParent.gameObject.SetActive(false);
                defenceParent.gameObject.SetActive(true);
                economicParent.gameObject.SetActive(false);
                break;

            case NavigationTab.Economic:
                attackParent.gameObject.SetActive(false);
                defenceParent.gameObject.SetActive(false);
                economicParent.gameObject.SetActive(true);
                break;
            
        }
    }

    private void InitUpgrades(List<UpgradeData> upgrades, Transform parent)
    {
        foreach (var data in upgrades)
        {
            data.fromWorkshop = true;

            var u = Instantiate(btnUpgradePrefab, parent);
            u.Init(data);
        }
    }
}
