using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Navigation : MonoBehaviour
{
    [SerializeField] List<BtnNavigation> btns;

    public Action<NavigationTab> onTabClick;

    public void Init()
    {
        foreach (var btn in btns)
        {
            btn.Init();
            btn.onClick += Tab_Clicked;
        }

        DeactiveAllTopTabs();
    }

    private void Tab_Clicked(NavigationTab tab)
    {
        DeactiveAllTopTabs();

        onTabClick?.Invoke(tab);
    }

    public void Active(NavigationTab tab)
    {
        btns.Find(n => n.NavigationTab == tab).Active();
    }

    public void DeactiveAllTopTabs()
    {
        foreach (var item in btns)
        {
            item.Deactive();
        }
    }
}

public enum NavigationTab
{
    Battle,
    Workshop,

    Upgrades,
    Strategy,

    Attack,
    Defence,
    Economic
}
