using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System;

public class PanelAutoStrategy : MonoBehaviour
{
    [SerializeField] Button btnAddRule;
    [SerializeField] RuleView ruleViewPrefab;
    [SerializeField] BtnSelectUpgrade selectUpgradePrefab;
    [SerializeField] Transform rulesParent;
    [SerializeField] Transform upgradesParent;
    
    [SerializeField] GameObject panelUpgrades;

    List<UpgradeData> upgradesAttack;
    List<UpgradeData> upgradesDefence;
    List<UpgradeData> upgradesEconomic;
    
    public List<RuleView> rules;


    RuleView lastRuleViewClicked;

    public void Init(List<UpgradeData> attack, List<UpgradeData> defence, List<UpgradeData> economic)
    {
        rules = new List<RuleView>();

        upgradesAttack = attack;
        upgradesDefence = defence;
        upgradesEconomic = economic;

        InitUpgrades();

        btnAddRule.onClick.AddListener(AddRule_Clicked);

        panelUpgrades.SetActive(false);
    }


    private void InitUpgrades()
    {
        List<UpgradeData> allUpgrades = new List<UpgradeData>();
        allUpgrades.Add(new UpgradeData { name = UpgradeName.Any });
        allUpgrades.AddRange(upgradesAttack);
        allUpgrades.AddRange(upgradesDefence);
        allUpgrades.AddRange(upgradesEconomic);

        foreach (var item in allUpgrades)
        {
            var u = Instantiate(selectUpgradePrefab, upgradesParent);
            u.Init(item);
            u.onSelect += Upgrade_Selected;
        }
    }

    private void Upgrade_Selected(UpgradeData data)
    {
        panelUpgrades.SetActive(false);

        lastRuleViewClicked.SetUpgrade(data);
    }

    private void AddRule_Clicked()
    {
        var rule = Instantiate(ruleViewPrefab, rulesParent);
        rule.Init();
        rule.onSelectUpgrade += SelectUpgrade_Clicked;
        rule.onDelete += Rule_Deleted;

        rules.Add(rule);

        btnAddRule.transform.SetAsLastSibling();
    }

    private void Rule_Deleted(RuleView rule)
    {
        rules.Remove(rule);
    }

    private void SelectUpgrade_Clicked(RuleView ruleView)
    {
        panelUpgrades.SetActive(true);

        lastRuleViewClicked = ruleView;
    }
}
