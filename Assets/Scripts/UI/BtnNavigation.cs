using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class BtnNavigation : MonoBehaviour
{
    [SerializeField] Button button;
    [SerializeField] TMP_Text txtTitle;
    [SerializeField] NavigationTab navigationTab;
    [SerializeField] Image image;
    [SerializeField] Color deactiveColor;

    public NavigationTab NavigationTab => navigationTab;

    Color activeColor;
    Color activeTxtColor;

    public Action<NavigationTab> onClick;

    public void Init()
    {
        button.onClick.AddListener(Button_Clicked);
        txtTitle.text = navigationTab.ToString();
        activeTxtColor = txtTitle.color;

        activeColor = image.color;
    }

    private void Button_Clicked()
    {
        onClick?.Invoke(navigationTab);

        Active();
    }

    public void Active()
    {
        image.color = activeColor;
        txtTitle.color = activeTxtColor;
    }

    public void Deactive()
    {
        image.color = deactiveColor;
        txtTitle.color = Color.gray;
    }
}
