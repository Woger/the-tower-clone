using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System;

public class PanelUpgrades : MonoBehaviour
{
    [SerializeField] ScrollRect scrollRect;
    [SerializeField] Transform attacksParent;
    [SerializeField] Transform defenceParent;
    [SerializeField] Transform economicParent;

    [Space]

    [SerializeField] List<BtnTabUpgrade> btnTabs;

    [SerializeField] BtnUpgrade upgradePrefab;
    [SerializeField] List<UpgradeData> upgrades;
    [SerializeField] List<UpgradeData> upgradesDefence;
    [SerializeField] List<UpgradeData> upgradesEconomic;

    TowerParameters tower;

    public List<UpgradeData> UpgradesAttack => upgrades;
    public List<UpgradeData> UpgradesDefence => upgradesDefence;
    public List<UpgradeData> UpgradesEconomic => upgradesEconomic;

    public List<BtnUpgrade> allBtnsUpgrade;

    public void Init(TowerParameters towerParameters)
    {
        allBtnsUpgrade = new List<BtnUpgrade>();

        InitTabs();

        Tab_Clicked(TabUpgrade.Attack);

        tower = towerParameters;

        InitUpgrades(upgrades, attacksParent);
        InitUpgrades(upgradesDefence, defenceParent);
        InitUpgrades(upgradesEconomic, economicParent);
    }

    private void InitUpgrades(List<UpgradeData> upgrades, Transform parent)
    {
        foreach (var data in upgrades)
        {
            data.tower = tower;
            var upgrade = Instantiate(upgradePrefab, parent);
            upgrade.Init(data);

            allBtnsUpgrade.Add(upgrade);
        }
    }

    private void InitTabs()
    {
        foreach (var tab in btnTabs)
        {
            tab.Init();
            tab.onTabClick += Tab_Clicked;
        }
    }

    private void Tab_Clicked(TabUpgrade type)
    {
        switch (type)
        {
            case TabUpgrade.Attack:
                scrollRect.content = (RectTransform)attacksParent;

                attacksParent.gameObject.SetActive(true);
                defenceParent.gameObject.SetActive(false);
                economicParent.gameObject.SetActive(false);
                break;

            case TabUpgrade.Defence:
                scrollRect.content = (RectTransform)defenceParent;

                attacksParent.gameObject.SetActive(false);
                defenceParent.gameObject.SetActive(true);
                economicParent.gameObject.SetActive(false);
                break;

            case TabUpgrade.Economic:
                scrollRect.content = (RectTransform)economicParent;

                attacksParent.gameObject.SetActive(false);
                defenceParent.gameObject.SetActive(false);
                economicParent.gameObject.SetActive(true);
                break;
            
        }
    }
}

[Serializable]
public class UpgradeData
{
    public UpgradeName name;
    public string description;
    public int startPrice;
    public int price;
    public int lvl;
    public Progression progression;
    public float priceFactor;
    public float valueFactor;
    public float priceStep;
    public float valueStep;
    public float value;
    public float lvlMultiplier = 1f;

    [Space(10)]

    public int coinStartPrice;
    public int coinPrice;
    public int lvlWorkshop;
    public float coinFactor;

    [HideInInspector]
    public bool fromWorkshop;
    [HideInInspector]
    public TowerParameters tower;
}

[Serializable]
public class UpgradesPack
{
    public List<UpgradeData> attack;
    public List<UpgradeData> defence;
    public List<UpgradeData> economic;
}

public enum UpgradeName
{
    Attack,
    AttackSpeed,
    CritChance,
    CritHitRatio,
    Distance,
    DamageByDistance,

    Health,
    Regen,

    MoneyMultiplier,
    MoneyWave,

    // For Auto Strategy
    Any
}

public enum Progression
{
    Linear,
    Exp,
    Exp_2
}
