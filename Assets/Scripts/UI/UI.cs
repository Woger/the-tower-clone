using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System;

public class UI : MonoBehaviour
{
    [SerializeField] Button btnComplete;

    [Space]

    [SerializeField] TowerParameters towerParameters;
    [SerializeField] Player player;
    [SerializeField] PanelUpgrades panelUpgrades;
    [SerializeField] PanelAutoStrategy panelAutoStrategy;
    [SerializeField] GameObject panelPizdos;
    [SerializeField] TMP_Text txtHealthPoint;
    [SerializeField] TMP_Text txtMoney;
    [SerializeField] TMP_Text txtWave;
    [SerializeField] TMP_Text txtCoins;
    [SerializeField] List<BtnNavigationTop> navigationsTop;

    [Space]

    [SerializeField] Button btnAuto;
    [SerializeField] Image autoOutline;

    List<UpgradeData> allUpgrades;

    Color autoEnabledColor;
    float autoModeTimer;
    bool autoMode;

    private void Start()
    {
        if (!Menu.isStarted)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            return;
        }

        panelUpgrades.Init(towerParameters);
        panelAutoStrategy.Init(panelUpgrades.UpgradesAttack, panelUpgrades.UpgradesDefence, panelUpgrades.UpgradesEconomic);

        panelAutoStrategy.gameObject.SetActive(false);
        panelPizdos.SetActive(false);

        InitNavigations();

        btnAuto.onClick.AddListener(BtnAuto_Clicked);
        btnComplete.onClick.AddListener(BtnComplete_Clicked);
        EventsHolder.onNextWave.AddListener(Wave_Nexted);
        EventsHolder.onTowerDestroyed.AddListener(Tower_Destroyed);

        autoEnabledColor = autoOutline.color;
        autoOutline.color = Color.gray;

        allUpgrades = new List<UpgradeData>();
        allUpgrades.AddRange(panelUpgrades.UpgradesAttack);
        allUpgrades.AddRange(panelUpgrades.UpgradesDefence);
        allUpgrades.AddRange(panelUpgrades.UpgradesEconomic);
    }

    private void BtnComplete_Clicked()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    private void Tower_Destroyed(Enemy destroyer)
    {
        panelPizdos.SetActive(true);
    }

    private void Wave_Nexted(int wave)
    {
        txtWave.text = $"Wave {wave}";
    }

    private void BtnAuto_Clicked()
    {
        autoMode = !autoMode;

        autoOutline.color = autoMode ? autoEnabledColor : Color.gray;
    }



    private void Update()
    {
        UpdateView();
        AutoMode();
    }

    private void UpdateView()
    {
        txtHealthPoint.text = $"{towerParameters.CurrentHealthPoint:F0}/{towerParameters.healthPoint:F0}";
        txtMoney.text = $"${Player.money}";
        txtCoins.text = $"Coins {Player.data.coins}";
    }

    private void AutoMode()
    {
        if (!autoMode)
        {
            return;
        }

        autoModeTimer += Time.deltaTime;

        if (autoModeTimer < 3)
            return;

        foreach (var rule in panelAutoStrategy.rules)
        {
            if (rule.UpgradeData == null)
                continue;

            if(rule.UpgradeData.name == UpgradeName.Any)
            {
                foreach (var btnUpgrade in panelUpgrades.allBtnsUpgrade)
                {
                    btnUpgrade.Select();
                    autoModeTimer = 0;
                }

                return;
            }

            var btn = panelUpgrades.allBtnsUpgrade.Find(btn => btn.UpgradeData.name == rule.UpgradeData.name);
            if (btn)
            {
                btn.Select();
                autoModeTimer = 0;
                //print("�����");
            }
        }
    }

    private void InitNavigations()
    {
        foreach (var item in navigationsTop)
        {
            item.Init();
            item.onTabClick += TopTab_Clicked;
        }


        DeactiveAllTopTabs();
        navigationsTop.Find(n => n.NavigationTab == NavigationTab.Upgrades).Active();
    }

    private void TopTab_Clicked(NavigationTab tab)
    {
        switch (tab)
        {
            case NavigationTab.Upgrades:
                panelAutoStrategy.gameObject.SetActive(false);
                break;

            case NavigationTab.Strategy:
                panelAutoStrategy.gameObject.SetActive(true);
                break;
        }

        DeactiveAllTopTabs();
    }

    private void DeactiveAllTopTabs()
    {
        foreach (var item in navigationsTop)
        {
            item.Deactive();
        }
    }
}
