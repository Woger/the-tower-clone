using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class BtnNavigationTop : MonoBehaviour
{
    [SerializeField] Button btn;
    [SerializeField] TMP_Text txtTitle;
    [SerializeField] NavigationTab navigationTab;
    [SerializeField] Image image;
    [SerializeField] Color deactiveColor;

    public NavigationTab NavigationTab => navigationTab;

    Color activeColor;

    public Action<NavigationTab> onTabClick;

    public void Init()
    {
        btn.onClick.AddListener(Tab_Clicked);
        txtTitle.text = navigationTab.ToString();

        activeColor = image.color;
    }

    private void Tab_Clicked()
    {
        onTabClick?.Invoke(navigationTab);

        Active();
    }

    public void Active()
    {
        image.color = activeColor;
    }

    public void Deactive()
    {
        image.color = deactiveColor;
    }
}


