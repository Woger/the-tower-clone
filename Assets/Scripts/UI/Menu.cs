using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class Menu : MonoBehaviour
{
    [SerializeField] TMP_Text txtCoins;
    [SerializeField] TMP_Text txtMaxWave;
    [SerializeField] Button btnBattle;
    [SerializeField] Navigation menuNavigation;
    [SerializeField] Navigation workshopNavigation;
    [SerializeField] GameObject panelBattle;
    [SerializeField] PanelWorkshop panelWorkshop;

    [Space(18)]

    [SerializeField] List<UpgradeData> attack;
    [SerializeField] List<UpgradeData> defence;
    [SerializeField] List<UpgradeData> economic;

    UpgradesPack pack;

    public static bool isStarted;

    private void Awake()
    {
        Player.LoadData();

        isStarted = true;
    }

    private void Start()
    {
        btnBattle.onClick.AddListener(() => SceneManager.LoadScene(1));

        menuNavigation.Init();
        menuNavigation.Active(NavigationTab.Battle);
        menuNavigation.onTabClick += Tab_Clicked;

        workshopNavigation.Init();
        workshopNavigation.Active(NavigationTab.Attack);
        workshopNavigation.onTabClick += WorkshopTab_Clicked;

        panelBattle.SetActive(true);
        panelWorkshop.gameObject.SetActive(false);

        InitUpgradesPack();

        panelWorkshop.Init(pack);

        txtMaxWave.text = $"Max Wave {Player.data.maxWave}";
    }

    private void WorkshopTab_Clicked(NavigationTab tab)
    {
        panelWorkshop.SwitchTab(tab);
    }

    private void Tab_Clicked(NavigationTab tab)
    {
        switch (tab)
        {
            case NavigationTab.Battle:
                panelBattle.SetActive(true);
                panelWorkshop.gameObject.SetActive(false);
                break;

            case NavigationTab.Workshop:
                panelBattle.SetActive(false);
                panelWorkshop.gameObject.SetActive(true);
                break;
            
            default:
                break;
        }
    }

    private void InitUpgradesPack()
    {
        pack = new UpgradesPack 
        {
            attack = attack,
            defence = defence,
            economic = economic
        };
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            PlayerPrefs.DeleteAll();
            Player.data = new PlayerData();
            SceneManager.LoadScene(0);
        }

        txtCoins.text = $"Coins {Player.data.coins}";
    }
}
