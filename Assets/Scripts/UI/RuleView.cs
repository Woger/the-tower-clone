using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class RuleView : MonoBehaviour
{
    [SerializeField] Button btnSelectUpgrade;
    [SerializeField] Button btnDelete;
    [SerializeField] TMP_Text txtUpgradeName;

    public Action<RuleView> onSelectUpgrade;
    public Action<RuleView> onDelete;
    public UpgradeData UpgradeData { get; private set; }

    public void Init()
    {
        btnSelectUpgrade.onClick.AddListener(SelectUpgrade_Clicked);
        btnDelete.onClick.AddListener(BtnDelete_Clicked);
    }

    private void BtnDelete_Clicked()
    {
        onDelete?.Invoke(this);

        Destroy(gameObject);
    }

    private void SelectUpgrade_Clicked()
    {
        onSelectUpgrade?.Invoke(this);
    }

    public void SetUpgrade(UpgradeData data)
    {
        txtUpgradeName.text = data.name.ToString();

        UpgradeData = data;
    }
}
