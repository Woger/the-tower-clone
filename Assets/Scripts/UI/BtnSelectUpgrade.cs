using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class BtnSelectUpgrade : MonoBehaviour
{
    [SerializeField] Button btn;
    [SerializeField] TMP_Text txtName;

    public Action<UpgradeData> onSelect;

    public void Init(UpgradeData data)
    {
        txtName.text = data.name.ToString();

        btn.onClick.AddListener(() => onSelect?.Invoke(data));
    }
}
