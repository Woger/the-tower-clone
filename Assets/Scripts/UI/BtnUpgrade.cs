using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class BtnUpgrade : MonoBehaviour
{
    [SerializeField] Button button;
    [SerializeField] TMP_Text txtName;
    [SerializeField] TMP_Text txtValue;
    [SerializeField] TMP_Text txtPrice;

    public Action<UpgradeData> onUpgradeClick;

    public UpgradeData UpgradeData { get; private set; }

    public void Init(UpgradeData upgradeData)
    {
        this.UpgradeData = upgradeData;

        if (upgradeData.fromWorkshop)
        {
            button.onClick.AddListener(UpgradeWorkshop_Clicked);
            EventsHolder.onMoneyChanged.AddListener(Money_Changed);
        }
        else
        {
            button.onClick.AddListener(BtnUpgrade_Clicked);
            EventsHolder.onMoneyChanged.AddListener(Money_Changed);
            
        }

        ApplyWorkshopUpgrades();

        UpdateView();
    }

    private void ApplyWorkshopUpgrades()
    {
        var data = Player.data.GetData(UpgradeData);

        if (data == null)
            return;

        var d = UpgradeData;

        for (int i = 1; i < data.lvl; i++)
        {
            if (d.progression == Progression.Exp)
            {
                d.value += d.valueStep + Mathf.Pow(i, d.valueFactor);
            }
            else if (d.progression == Progression.Linear)
            {
                d.value += d.valueStep;
            }
            else if (d.progression == Progression.Exp_2)
            {
                d.value += d.lvlMultiplier * (d.valueStep + Mathf.Pow(i, d.valueFactor));
            }
            
        }

        if (UpgradeData.fromWorkshop)
        {
            UpgradeData.lvlWorkshop = data.lvl;

            for (int i = 1; i < data.lvl; i++)
            {
                d.coinPrice += d.coinStartPrice + (int)Mathf.Pow(2 + i, d.coinFactor);
            }
        }
        else
        {
            UpdateParameter();
        }

        
    }

   
    private void Money_Changed()
    {
        UpdateView();
    }

    public void Select()
    {
        button.onClick.Invoke();
    }

    private void UpgradeWorkshop_Clicked()
    {
        var d = UpgradeData;

        if (Player.data.coins >= d.coinPrice)
        {
            Player.data.coins -= d.coinPrice;

            d.coinPrice += d.coinStartPrice + (int)Mathf.Pow(2 + d.lvlWorkshop, d.coinFactor);
            //d.lvlWorkshop++;

            if (d.progression == Progression.Exp)
            {
                d.value += d.valueStep + Mathf.Pow(d.lvlWorkshop, d.valueFactor);
            }
            else if (d.progression == Progression.Linear)
            {
                d.value += d.valueStep;
            }
            else if (d.progression == Progression.Exp_2)
            {
                d.value += d.lvlMultiplier * (d.valueStep + Mathf.Pow(d.lvlWorkshop, d.valueFactor));
            }

            d.lvlWorkshop++;

            Player.data.SetUpgrade(d);
            Player.SaveData();

            EventsHolder.onMoneyChanged?.Invoke();
        }
    }


    public void BtnUpgrade_Clicked()
    {
        //print("�� ������ �����?");

        onUpgradeClick?.Invoke(UpgradeData);

        var d = UpgradeData;

        if(Player.money >= d.price)
        {
            Player.money -= d.price;
            

            d.price += (int)d.priceStep + (int)Mathf.Pow(d.lvl, d.priceFactor);//Mathf.Pow(d.lvl, d.stepFactor);

            if (d.progression == Progression.Exp)
            {
                d.value += d.valueStep + Mathf.Pow(d.lvl, d.valueFactor);
            }
            else if (d.progression == Progression.Linear)
            {
                d.value += d.valueStep;
            }
            else if (d.progression == Progression.Exp_2)
            {
                d.value += d.lvlMultiplier * (d.valueStep + Mathf.Pow(d.lvl, d.valueFactor));
            }

            UpdateParameter();

            d.lvl++;

            EventsHolder.onMoneyChanged?.Invoke();
        }

        //d.price = (int)(d.startPrice * Mathf.Pow(d.stepFactor, d.lvl));
        //d.price = (int)((float)d.startPrice * Mathf.Pow(d.lvl, d.stepFactor));
        //d.price = (int)(d.stepFactor * Mathf.Pow(d.startPrice, d.lvl));
        //d.price = (int)(Mathf.Pow(d.startPrice * d.lvl, d.stepFactor));
    }

    private void UpdateParameter()
    {
        switch (UpgradeData.name)
        {
            case UpgradeName.Attack:
                UpgradeData.value = (int)UpgradeData.value;
                UpgradeData.tower.damage = (int)UpgradeData.value;
                break;

            case UpgradeName.AttackSpeed:
                UpgradeData.tower.speedAtack = UpgradeData.value;
                break;

            case UpgradeName.CritChance:
                UpgradeData.tower.critChance = UpgradeData.value;
                break;

            case UpgradeName.CritHitRatio:
                UpgradeData.tower.critHitRatio = UpgradeData.value;
                break;

            case UpgradeName.Distance:
                UpgradeData.tower.distance = UpgradeData.value;
                break;

            // ===== HEALTH POINT =====
            case UpgradeName.Health:
                var tower = UpgradeData.tower;
                var difference = UpgradeData.value - tower.healthPoint;

                UpgradeData.value = (int)UpgradeData.value;
                tower.healthPoint = (int)UpgradeData.value;
                tower.CurrentHealthPoint += (int)difference;
                break;

            case UpgradeName.Regen:
                UpgradeData.tower.healthRegen = UpgradeData.value;
                break;

            // ===== ECONOMIC =====
            case UpgradeName.MoneyMultiplier:
                Player.moneyMultiplier = UpgradeData.value;
                break;

            case UpgradeName.MoneyWave:
                Player.moneyWave = (int)UpgradeData.value;
                break;
        }
    }

    private void UpdateView()
    {
        txtName.text = UpgradeData.name.ToString();
        txtValue.text = UpgradeData.value.ToString("F2");
        

        if (UpgradeData.fromWorkshop)
        {
            txtPrice.text = UpgradeData.coinPrice.ToString();

            button.interactable = Player.data.coins >= UpgradeData.coinPrice;
        }
        else
        {
            txtPrice.text = UpgradeData.price.ToString();

            button.interactable = Player.money >= UpgradeData.price;
        }

        
        
    }
}
