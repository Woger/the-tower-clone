using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerParameters : MonoBehaviour
{
    public int damage = 3;
    public float speedAtack = 1.0f;
    public float critChance = 0;
    public float critHitRatio = 1.2f;
    public float distance = 3f;
    public float healthPoint = 5;
    public float healthRegen = 0;

    public float CurrentHealthPoint { get; set; }

    float regenTimer;

    private void Start()
    {
        CurrentHealthPoint = healthPoint;
    }


    private void Update()
    {
        if (CurrentHealthPoint < healthPoint)
        {
            CurrentHealthPoint += healthRegen * Time.deltaTime;
        }
            
        CurrentHealthPoint = Mathf.Clamp(CurrentHealthPoint, 0, healthPoint);
    }

    public void Damage(Enemy enemy)
    {
        CurrentHealthPoint -= enemy.damage;

        if(CurrentHealthPoint <= 0)
        {
            EventsHolder.onTowerDestroyed?.Invoke(enemy);
        }
    }
}
