using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Tower : MonoBehaviour
{
    [SerializeField] TowerParameters towerParameters;
    [SerializeField] Projectile projectilePrefab;
    [SerializeField] float fireRate = 1f;
    
    [SerializeField] GameObject radiusAttackIndicator;

    public TowerParameters Parameters => towerParameters;

    GameManager gameManager;
    Enemy target;
    float currentRate;

    public void Init(GameManager da)
    {
        gameManager = da;


    }


    private void Update()
    {
        UpdateRadiusIndicator();

        currentRate += Time.deltaTime * towerParameters.speedAtack;

        if (currentRate < fireRate)
            return;

        gameManager.enemies.RemoveAll(e => !e);

        if (gameManager.enemies.Count > 0)
        {

            var minDist = gameManager.enemies.Min(e => e.DistanceToTower);
            
            if (minDist < towerParameters.distance + 0.5f && minDist > 0.1f)
            {
                var enemy = gameManager.enemies.Find(e => Mathf.Approximately(minDist, e.DistanceToTower));

                enemy.GetComponentInChildren<SpriteRenderer>().color = Color.blue;

                var p = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
                p.Init(enemy, towerParameters);

                currentRate = 0;
            }
        }
    }

    void UpdateRadiusIndicator()
    {
        radiusAttackIndicator.transform.localScale = Vector3.one * towerParameters.distance * 2;
    }
}
