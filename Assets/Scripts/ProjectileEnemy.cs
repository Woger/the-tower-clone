using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEnemy : MonoBehaviour
{
    [SerializeField] float speed = 3f;
    Tower target;
    Vector2 dir;


    float lifetime;

    public Enemy Owner { get; private set; }

    public void Init(Tower target, Enemy owner)
    {
        Owner = owner;
        this.target = target;

        dir = (target.transform.position - transform.position).normalized;
    }

    private void Update()
    {
        lifetime += Time.deltaTime;

        transform.position += speed * Time.deltaTime * (Vector3)dir;

        if (target)
        {

            var dist = Vector2.Distance(transform.position, target.transform.position);
            if (dist < 0.3f)
            {
                Destroy(gameObject);

                target.Parameters.Damage(Owner);
            }
        }

        if (lifetime > 8)
        {
            Destroy(gameObject);
        }
    }
}
