using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speed = 3f;
    Enemy target;
    Vector2 dir;
    int damage;

    float lifetime;

    public void Init(Enemy target, TowerParameters tower)
    {
        damage = tower.damage;

        if(Random.Range(0f, 100f) < tower.critChance)
        {
            damage = (int)((float)damage * tower.critHitRatio);
        }

        this.target = target;

        dir = (target.transform.position - transform.position).normalized;
    }

    private void Update()
    {
        lifetime += Time.deltaTime;

        transform.position += speed * Time.deltaTime * (Vector3)dir;

        if (target)
        {

            var dist = Vector2.Distance(transform.position, target.transform.position);
            if(dist < 0.3f)
            {
                Destroy(gameObject);

                target.HealthPoint -= damage;

                if (target.HealthPoint <= 0)
                {
                    EventsHolder.onEnemyDestroyed?.Invoke(target);

                    Destroy(target.gameObject);
                }
            }
        }

        if(lifetime > 8)
        {
            Destroy(gameObject);
        }
    }
}
